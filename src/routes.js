/* USERS CONTROLLERS */
const CreateUser = require('./controllers/user/create.js')
const ShowUser = require('./controllers/user/show.js')
const SearchUser = require('./controllers/user/search.js')
const UpdateUser = require('./controllers/user/update.js')
const DeleteUser = require('./controllers/user/delete.js')
const ConnectUser = require('./controllers/user/connect.js')

/* MEDIAS CONTROLLERS */
const ShowMedia = require('./controllers/medias/show.js')

/* EVENTS CONTROLLERS */
const CreateEvent = require('./controllers/event/create.js')
const ShowEvent = require('./controllers/event/show.js')
const UpdateEvent = require('./controllers/event/update.js')
const GetListEvent = require('./controllers/event/getList.js')
const DeleteEvent = require('./controllers/event/delete.js')

/* CATOGERIES CONTROLLERS */
const CreateCategory = require('./controllers/category/create.js')
const ShowCategory = require('./controllers/category/show.js')
const GetListCategory = require('./controllers/category/getList.js')
const DeleteCategory = require('./controllers/category/delete.js')

/* EVENT_CATEGORIES CONTROLLERS */
const CreateEventCategories = require('./controllers/event_categories/create.js')
const DeleteEventCategories = require('./controllers/event_categories/delete.js')
const GetListEventCategories = require('./controllers/event_categories/getList.js')

/* USER_CATEGORIES CONTROLLERS */
const CreateUserCategories = require('./controllers/user_categories/create.js')
const DeleteUserCategories = require('./controllers/user_categories/delete.js')
const GetListUserCategories = require('./controllers/user_categories/getList.js')

/* USER_EVENTS CONTROLLERS */
const CreateUserEvents = require('./controllers/user_events/create.js')
const DeleteUserEvents = require('./controllers/user_events/delete.js')
const GetListUserEvents = require('./controllers/user_events/getList.js')

/* MESSAGES CONTROLLERS */
const CreateMessage = require('./controllers/messages/create.js')
const GetListMessage = require('./controllers/messages/getList.js')

module.exports = {
  user: {
    CreateUser,
    ShowUser,
    SearchUser,
    UpdateUser,
    DeleteUser,
    ConnectUser
  },
  media: {
    ShowMedia
  },
  event: {
    CreateEvent,
    ShowEvent,
    UpdateEvent,
    GetListEvent,
    DeleteEvent
  },
  category: {
    CreateCategory,
    ShowCategory,
    GetListCategory,
    DeleteCategory
  },
  event_categories: {
    CreateEventCategories,
    DeleteEventCategories,
    GetListEventCategories
  },
  user_categories: {
    CreateUserCategories,
    DeleteUserCategories,
    GetListUserCategories
  },
  user_events: {
    CreateUserEvents,
    DeleteUserEvents,
    GetListUserEvents
  },
  messages: {
    CreateMessage,
    GetListMessage
  }
}
const connexion = require('../../config/connexion')

const sql = {
  mustHave: {
    name: ''
  },
  async create(name) {
    const queryString = `insert into category (name) values (?)`
    return await connexion.query(queryString, [name])
  },
  async show(id) {
    const queryString = `select * from category where id = ?`
    return await connexion.query(queryString, [id])
  },
  async getList() {
    const queryString = `select * from category`
    return await connexion.query(queryString)
  },
  async delete(id) {
    const queryString = `delete from category where id = ?`
    return await connexion.query(queryString, [id])
  }
}

module.exports = { sql }

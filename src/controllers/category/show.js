const { sql } = require('./sql')

class Show {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/categories/:id', this.callback)
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			resultJson.status = 200
			resultJson.data = await sql.show(req.params.id)
			res.status(200).send(resultJson)
		} catch (exception) {
			resultJson.status = 500
			resultJson.data = null
			resultJson.errors.push(`Une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Show
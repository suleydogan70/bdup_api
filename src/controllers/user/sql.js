const connexion = require('../../config/connexion')

const sql = {
	mustHave: {
		firstname: '',
		lastname: '',
		sexe: '',
		mail: '',
		password: '',
		birth_date: ''
	},
  async checkUserExist(mail, password) {
    const queryString = `select
      user.id,
      concat(firstname, ' ', lastname) as fullname,
      sexe,
      avatar,
      role,
      mail,
      birth_date,
      state,
      last_connexion 
      from user 
        inner join rank on rank.id = user.id_rank 
      where mail = ? and password = ?`
    return await connexion.query(queryString, [mail, password])
  },
  async update(user, id) {
  	const date = new Date()
    let queryString = `update user set`
    for (let query in user) {
    		queryString += ` ${query} = '${user[query]}' , `
    }
    queryString += `last_connexion = ? where id = ?`
    return await connexion.query(queryString, [
    	date.toISOString().slice(0, 19).replace('T', ' '),
    	id
    ])
  },
  async create(user) {
  	const date = new Date()
    let queryString = `insert into user (`
    for (let query in user) {
      queryString += ` ${query}, `
    }
    queryString += ' last_connexion) values ( '
    for (let query in user) {
      queryString += ` '${user[query]}', `
    }
    queryString += ` '${date.toISOString().slice(0, 19).replace('T', ' ')}')`
    return await connexion.query(queryString)
  },
  async search(queries) {
  	const date = new Date()
    let queryString = `select * from user where 1 = 1 `
    for (let query in queries) {
    	queryString += ` and ${query} like '%${queries[query]}%' `
    }
    return await connexion.query(queryString)
  },
  async show(id) {
    let queryString = `
      select
      user.id,
      concat(user.firstname, ' ', user.lastname) as fullname,
      user.sexe,
      user.avatar,
      user.role,
      user.mail,
      user.birth_date,
      user.state,
      user.last_connexion,
      rank.name
      from user
        inner join rank on rank.id = user.id_rank
      where user.id = ?`
    return await connexion.query(queryString, [id])
  },
  async delete(id) {
    let queryString = `delete from user where id = ?`
    return await connexion.query(queryString, [id])
  }
}

module.exports = { sql }

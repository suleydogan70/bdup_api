const { sql } = require('./sql')

class Search {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.get('/users/search', this.callback)
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			resultJson.status = 200
			resultJson.data = await sql.search(req.query)
			res.status(200).send(resultJson)
		} catch (exception) {
			resultJson.status = 500
			resultJson.data = null
			resultJson.errors.push(`La recherche n'a pas pu aboutir à un résultat`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Search
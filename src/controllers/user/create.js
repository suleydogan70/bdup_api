const { sql } = require('./sql')
const JWT = require('jsonwebtoken')

class Create {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/users/create', this.callback.bind(this))
	}

	signToken (user) {
	  return JWT.sign({
	    iss: 'Splitziz',
	    sub: user
	  }, 'SPLITZIZSECRETKEY')
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		for (let mustHave in sql.mustHave) {
			if (typeof req.body[mustHave] === 'undefined') {
				resultJson.errors.push('Information(s) manquante(s)')
				resultJson.status = 300
				res.status(200).send(resultJson)
				return
			}
		}
		try {
			if (req.file) {
				req.body.avatar = req.file.filename
			}
			const insertion = await sql.create(req.body)
			const user = await sql.show(insertion.insertId)
			resultJson.status = 200
			resultJson.success.push(`L'utilisateur ${req.body.firstname || ''} ${req.body.lastname || ''} a bien été créé`)
			resultJson.data = {
				user: user[0],
				token: this.signToken(user[0])
			}
			res.status(200).send(resultJson)
		} catch (exception) {
			console.log(exception)
			resultJson.status = 500
			resultJson.success = []
			resultJson.errors.push(`L'enregistrement a échoué, une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Create

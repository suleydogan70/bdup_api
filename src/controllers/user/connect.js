const { sql } = require('./sql')
const JWT = require('jsonwebtoken')
const bcrypt = require('bcrypt')

class Connect {
  constructor (app) {
    this.app = app

    this.run()
  }

  signToken (user) {
    return JWT.sign({
      iss: 'Splitziz',
      sub: user
    }, 'SPLITZIZSECRETKEY')
  }

  verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

  middleware () {
    this.app.post('/user/connect', this.verifyToken, this.callback.bind(this))
  }

  async callback (req, res) {
    const resultJson = {
      success: [],
      errors: [],
      status: null
    }
    const { mail, password } = req.body
    const { token } = req
    if (token) {
      JWT.verify(token, 'SPLITZIZSECRETKEY', async (err, data) => {
        if (err) {
          return res.sendStatus(403)
        }
        const user = await sql.show(data.sub.id)
        resultJson.data = {
          user: user[0],
          token
        }
        resultJson.status = 200
        res.status(200).send(resultJson)
      })
    } else {
      if (mail === '' || password === '') {
        resultJson.errors.push('Information(s) manquante(s)')
        resultJson.status = 300
        res.status(200).send(resultJson)
        return
      }
      
      try {
        const user = await sql.checkUserExist(mail, password)
        if (user.length == 0) {
          resultJson.errors.push('Mail ou mot de passe incorrect')
          resultJson.status = 404
          res.status(200).send(resultJson)
        } else {
          resultJson.status = 200
          resultJson.data = {
            user: user[0],
            token: this.signToken(user[0])
          }
          res.status(200).send(resultJson)
        }
      } catch (exception) {
        resultJson.status = 500
        resultJson.data = null
        resultJson.errors.push(`La recherche n'a pas pu aboutir à un résultat`)
        res.status(500).send(resultJson)
      }
    }
  }

  run () {
    this.middleware()
  }
}

module.exports = Connect
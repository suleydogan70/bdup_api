const connexion = require('../../config/connexion')

const sql = {
  async create(id_event, id_user) {
    const queryString = `insert into user_events (id_event, id_user) values (?, ?)`
    return await connexion.query(queryString, [id_event, id_user])
  },
  async checkExist(id_event, id_user) {
    const queryString = `select 1 from user_events where id_event = ? and id_user = ?`
    return await connexion.query(queryString, [id_event, id_user])
  },
  async getList(id_user) {
    const queryString = `
      select event.*
      from event
      inner join user_events on event.id = user_events.id_event
      where user_events.id_user = ?`
    return await connexion.query(queryString, [id_user])
  },
  async delete(id_event, id_user) {
    const queryString = `delete from user_events where id_user = ? and id_event = ?`
    return await connexion.query(queryString, [id_user, id_event])
  }
}

module.exports = { sql }

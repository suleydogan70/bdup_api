const { sql } = require('./sql')
const JWT = require('jsonwebtoken')

class Create {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/user_events/create', this.verifyToken, this.callback)
	}

	verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

	async callback (req, res) {
		const { token } = req
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			JWT.verify(token, 'SPLITZIZSECRETKEY', async (err, data) => {
        if (err) {
          return res.sendStatus(403)
        }
        const user_event = await sql.checkExist(req.body.id_event, data.sub.id)
        if (user_event.length === 0) {
					await sql.create(req.body.id_event, data.sub.id)
					resultJson.status = 200
					resultJson.success.push(`Les événements ont bien été créés`)
					res.status(200).send(resultJson)
        } else {
        	await sql.delete(req.body.id_event, data.sub.id)
					resultJson.status = 200
					resultJson.success.push(`Les événements ont bien été supprimées`)
					res.status(200).send(resultJson)
        }
      })
		} catch (exception) {
			resultJson.status = 500
			resultJson.success = []
			resultJson.errors.push(`L'enregistrement a échoué, une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Create
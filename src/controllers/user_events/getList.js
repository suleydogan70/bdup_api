const { sql } = require('./sql')
const JWT = require('jsonwebtoken')

class List {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/user_events/list', this.verifyToken, this.callback)
	}

	verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

	async callback (req, res) {
		const { token } = req
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			JWT.verify(token, 'SPLITZIZSECRETKEY', async (err, data) => {
        if (err) {
          return res.sendStatus(403)
        }
        resultJson.data = await sql.getList(data.sub.id)
        resultJson.status = 200
        res.status(200).send(resultJson)
      })
		} catch (exception) {
			resultJson.status = 500
			resultJson.data = null
			resultJson.errors.push(`Une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = List
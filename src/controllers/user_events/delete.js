const { sql } = require('./sql')

class Delete {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/user_events/delete', this.callback)
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			resultJson.status = 200
			resultJson.success.push(`la catégorie a bien été supprimé`)
			await sql.delete(req.body.id_user, req.body.id_category)
			res.status(200).send(resultJson)
		} catch (exception) {
			resultJson.status = 500
			resultJson.success = []
			resultJson.errors.push(`La suppression a échoué`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Delete
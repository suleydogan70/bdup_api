const fs = require('fs')

class Show {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.get('/medias/:folder/:img', this.callback)
	}

	async callback (req, res) {
		const { folder, img } = req.params
		try {
			const file = fs.readFileSync(`medias/${folder}/${img}`)
			res.send(file)
		} catch (exception) {
			res.status(404).send('Le fichier que vous cherchez n\'existe pas')
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Show
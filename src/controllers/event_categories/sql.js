const connexion = require('../../config/connexion')

const sql = {
  mustHave: {
    id_category: '',
    id_event: ''
  },
  async create({ id_category, id_event }) {
    const queryString = `insert into event_categories (id_category, id_event) values (?, ?)`
    return await connexion.query(queryString, [id_category, id_event])
  },
  async getList(id_event) {
    const queryString = `
      select category.name
      from event
      inner join event_categories on event.id = event_categories.id_event
      inner join category on event_categories.id_category = category.id
      where event_categories.id_event = ?`
    return await connexion.query(queryString, [id_event])
  },
  async delete(id) {
    const queryString = `delete from event_categories where id = ?`
    return await connexion.query(queryString, [id])
  }
}

module.exports = { sql }

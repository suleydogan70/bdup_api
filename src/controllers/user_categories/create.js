const { sql } = require('./sql')
const JWT = require('jsonwebtoken')

class Create {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/user_categories/create', this.verifyToken, this.callback)
	}

	verifyToken(req, res, next) {
    const { authorization } = req.headers
    if (authorization === undefined) {
      // res.status(403)
    } else {
      const bearer = authorization.split(' ')
      req.token = bearer[1]
    }
    next()
  }

	async callback (req, res) {
		const { token } = req
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			JWT.verify(token, 'SPLITZIZSECRETKEY', async (err, data) => {
        if (err) {
          return res.sendStatus(403)
        }
        for (var i = req.body.length - 1; i >= 0; i--) {
					await sql.create({ id_category: req.body[i].id, id_user: data.sub.id })
				}
				resultJson.data = await sql.getList(data.sub.id)
				resultJson.status = 200
				resultJson.success.push(`Les catégories ont bien été créés`)
        res.status(200).send(resultJson)
      })
		} catch (exception) {
			console.log(exception)
			resultJson.status = 500
			resultJson.success = []
			resultJson.errors.push(`L'enregistrement a échoué, une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Create
const connexion = require('../../config/connexion')

const sql = {
  mustHave: {
    id_user: '',
    id_category: ''
  },
  async create({ id_category, id_user }) {
    const queryString = `insert into user_categories (id_category, id_user) values (?, ?)`
    return await connexion.query(queryString, [id_category, id_user])
  },
  async getList(id_user) {
    const queryString = `
      select category.*
      from user
      inner join user_categories on user.id = user_categories.id_user
      inner join category on user_categories.id_category = category.id
      where user_categories.id_user = ?`
    return await connexion.query(queryString, [id_user])
  },
  async delete(id_event, id_user) {
    const queryString = `delete from user_categories where id_user = ? and id_event = ?`
    return await connexion.query(queryString, [id_user, id_event])
  }
}

module.exports = { sql }

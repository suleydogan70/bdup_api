const connexion = require('../../config/connexion')

const sql = {
  mustHave: {
    id_user: '',
    id_category: '',
    message: ''
  },
  async create({ id_user, id_category, message }) {
    const date = new Date()
    const queryString = `insert into message (id_user, id_category, content, date) values (?, ?, ?, ?)`
    return await connexion.query(queryString, [id_user, id_category, message, date.toISOString().slice(0, 19).replace('T', ' ')])
  },
  async getList(id_category) {
    const queryString = `
      select message.*, concat(user.firstname, ' ', user.lastname) as fullname, user.avatar
      from message
      inner join user on user.id = message.id_user
      where id_category = ?`
    return await connexion.query(queryString, [id_category])
  }
}

module.exports = { sql }

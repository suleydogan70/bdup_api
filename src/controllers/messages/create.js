const { sql } = require('./sql')

class Create {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/messages/create', this.callback)
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		for (let mustHave in sql.mustHave) {
			if (typeof req.body[mustHave] === 'undefined') {
				resultJson.errors.push('Information(s) manquante(s)')
				resultJson.status = 300
				res.status(300).send(resultJson)
				return
			}
		}
		try {
			await sql.create(req.body)
			resultJson.data = await sql.getList(req.body.id_category)
			resultJson.status = 200
			resultJson.success.push(`Message envoyé`)
			res.status(200).send(resultJson)
		} catch (exception) {
			console.log(exception)
			resultJson.status = 500
			resultJson.success = []
			resultJson.errors.push(`Le message s'est perdu dans l'internet ! Excusez nous pour la gêne occasionnée`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Create
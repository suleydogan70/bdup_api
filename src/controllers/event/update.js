const { sql } = require('./sql')

class Update {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/events/:id/update', this.callback)
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			await sql.update(req.body, req.params.id)
			resultJson.status = 200
			resultJson.success.push(`La modification a bien été prise en compte`)
			res.status(200).send(resultJson)
		} catch (exception) {
			resultJson.status = 500
			resultJson.success = []
			resultJson.errors.push(`L'enregistrement a échoué, une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = Update
const connexion = require('../../config/connexion')

const sql = {
  mustHave: {
    title: '',
    description: '',
    thumbnail: '',
    address: '',
    city: '',
    postal_code: '',
    date: ''
  },
  async update(event, id) {
    const date = new Date()
    let queryString = `update event set`
    for (let query in event) {
      queryString += ` ${query} = '${event[query]}' , `
    }
    queryString += `modification_date = ? where id = ?`
    return await connexion.query(queryString, [
      date.toISOString().slice(0, 19).replace('T', ' '),
      id
    ])
  },
  async create(event) {
    const date = new Date()
    let queryString = `insert into event (`
    for (let query in event) {
      queryString += ` ${query}, `
    }
    queryString += 'publish_date, modification_date) values ( '
    for (let query in event) {
      queryString += ` '${event[query]}', `
    }
    queryString += ` '${date.toISOString().slice(0, 19).replace('T', ' ')}', '${date.toISOString().slice(0, 19).replace('T', ' ')}' )`
    return await connexion.query(queryString)
  },
  async show(id) {
    const queryString = `select * from event where id = ?`
    return await connexion.query(queryString, [id])
  },
  async getList(category) {
    let queryString
    if (category) {
      queryString = `select * from event inner join event_categories on event_categories.id_event = event.id where event_categories.id_category = '${category}'`
    } else {
      queryString = `select * from event`
    }
    return await connexion.query(queryString)
  },
  async delete(id) {
    const queryString = `delete from event where id = ?`
    return await connexion.query(queryString, [id])
  }
}

module.exports = { sql }

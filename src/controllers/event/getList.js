const { sql } = require('./sql')

class List {
	constructor (app) {
		this.app = app

		this.run()
	}

	middleware () {
		this.app.post('/events/list', this.callback)
	}

	async callback (req, res) {
		const resultJson = {
			success: [],
			errors: [],
			status: null
		}
		try {
			resultJson.status = 200
			console.log(req.body)
			if (req.body.category === 0) {
				resultJson.data = await sql.getList()
			} else {
				resultJson.data = await sql.getList(req.body.category)
			}
			res.status(200).send(resultJson)
		} catch (exception) {
			resultJson.status = 500
			resultJson.data = null
			resultJson.errors.push(`Une erreur est survenue`)
			res.status(500).send(resultJson)
		}
	}

	run () {
		this.middleware()
	}
}

module.exports = List
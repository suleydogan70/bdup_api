const compression = require('compression')
const bodyParser = require('body-parser')
const express = require('express')
const helmet = require('helmet')
const multer = require('multer')
const cors = require('cors')
const path = require('path')

const routes = require('./routes.js')
const maxSize = 4 * 1000 * 1000;

class ApiBDUP {
    constructor () {
    	this.app = express()

    	this.run()
    }

    middleware () {
        const storage = multer.diskStorage({
          filename: function (req, file, cb) {
            console.log(file)
            cb(null, file.fieldname + '-' + Date.now())
          }
        })
        const upload = multer({
         limits:{
          fileSize: maxSize
         },
         fileFilter: function (req, file, cb) {
           const filetypes = /jpeg|jpg|png/
           const mimetype = filetypes.test(file.mimetype)
           const extname = filetypes.test(path.extname(file.originalname).toLowerCase())
           if (mimetype && extname && file.originalname === escape(file.originalname)) {
             return cb(null, true)
           }
           cb("Error: File upload not valid")
         },
         dest: 'medias/avatars/'
        })
        this.app.use(compression())
        this.app.use(cors())
        this.app.use(bodyParser.urlencoded({ 'extended': true }))
        this.app.use(bodyParser.json())
        this.app.use(upload.single('avatar'))
      }

	routes () {
        new routes.user.CreateUser(this.app)
        new routes.user.ShowUser(this.app)
        new routes.user.SearchUser(this.app)
        new routes.user.UpdateUser(this.app)
        new routes.user.DeleteUser(this.app)
        new routes.user.ConnectUser(this.app)

        new routes.media.ShowMedia(this.app)

        new routes.event.CreateEvent(this.app)
        new routes.event.GetListEvent(this.app)
        new routes.event.ShowEvent(this.app)
        new routes.event.UpdateEvent(this.app)
        new routes.event.DeleteEvent(this.app)

        new routes.category.CreateCategory(this.app)
        new routes.category.ShowCategory(this.app)
        new routes.category.GetListCategory(this.app)
        new routes.category.DeleteCategory(this.app)
        
        new routes.event_categories.CreateEventCategories(this.app)
        new routes.event_categories.DeleteEventCategories(this.app)
        new routes.event_categories.GetListEventCategories(this.app)
        
        new routes.user_categories.CreateUserCategories(this.app)
        new routes.user_categories.DeleteUserCategories(this.app)
        new routes.user_categories.GetListUserCategories(this.app)
        
        new routes.user_events.CreateUserEvents(this.app)
        new routes.user_events.DeleteUserEvents(this.app)
        new routes.user_events.GetListUserEvents(this.app)
        
        new routes.messages.CreateMessage(this.app)
        new routes.messages.GetListMessage(this.app)

    // If route not exist
        this.app.use((req, res) => {
          res.status(404).json({
            'code': 404,
            'message': 'Not Found'
          })
        })
    }

	security () {
        this.app.use(helmet())
        this.app.disable('x-powered-by')
    }

	run () {
		try {
	      this.security()
	      this.middleware()
	      this.routes()
	      this.app.listen(4000)
	    } catch (e) {
	      console.error(`[ERROR] Server -> ${e}`)
	    }
	}
}

module.exports = ApiBDUP
